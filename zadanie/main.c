#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int main()
{
    int choice = 0;
    char surname[30] = "";

    struct Client *head;
    head = (struct Client *)malloc(sizeof(struct Client *));
    strcpy(head->surname, "Kruszona");
    head->next = NULL;

    while (1)
    {
        while (choice > 5 || choice < 1)
        {
            printf("Wybierz jedna z ponizszych opcji\n Wcisnij 1, aby dodac nowego klienta\n Wcisnij 2, aby usunac istniejacego klienta\n Wcisnij 3, aby wyswietlic liczbe klientow\n Wcisnij 4, aby wyswietlic nazwiska klientow\n Wcisnij 5, aby zakonczyc program: ");
            scanf(" %d", &choice);
        }
        printf("%d", choice);
        switch (choice)
        {
        case 1:
            printf("Wybrano dodanie nowego klienta\n prosze podac nazwisko: ");
            scanf("%s", surname);
            push_back(&head, surname);
            break;

        case 2:
            printf("\n wybrano usuniecie klienta");
            surname[0] = "\0";
            printf("Podaj nazwisko klienta, ktorego chcesz usunac: ");
            scanf("%s", surname);
            pop_by_surname(&head, surname);
            break;

        case 3:
            printf("\n liczba klientow wynosi %d \n", list_size(head));
            break;

        case 4:
            printf("oto lista klientow: \n \n");
            show_list(head);
            break;
        case 5:
            free(head);
            exit(1);
            break;
        }
        choice = 0;
    }
    free(head);
    return 0;
}